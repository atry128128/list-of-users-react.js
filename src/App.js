import React, { Component } from "react";
import User from "./User.js";
import UserList from "./UserList.js";
import Form from "./Form.js";
import "./styles.css";
import { Modal, Button, Alert } from "react-bootstrap";


class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      users: [],
      showListUsers: true,
      showModal: false,
      showEditForm: false,
      idToDelete: -1,
      idToEdit: -1,
      userToEdit: "",
      nameUserToDelete: "",
      typeForm: "",
      isLoadingData:false
    };
  }

  componentWillMount() {
    const APIurl = "https://jsonplaceholder.typicode.com/users";

    fetch(APIurl)
      .then(response => response.json())
      .then(date => {
        this.setState({
          users: date,
          isLoadingData: true
        });
      });
  }

  openEditForm = user => {
    this.setState({
      showListUsers: false,
      userToEdit: user,
      idToEdit: user.id,
      typeForm: "edit"
    });
  }

  handleCancelClick() {
    this.setState({
      showListUsers: !this.state.showListUsers
    });
  }

  handleAddClick = () => {
    this.setState({
      showListUsers: !this.state.showListUsers,
      typeForm: "add"
    });
  };

  openDeleteModal = id => {
    const userToDelete = this.state.users.filter(user => user.id === id);

    this.setState({
      showModal: true,
      idToDelete: id,
      nameUserToDelete: userToDelete[0].name
    });
  };

  deleteUser = () => {
    const newListOfUsers = this.state.users.filter(user => user.id !== this.state.idToDelete);
    this.setState({
      users: newListOfUsers,
      showModal: false,

    })
  }

  closeDeleteModal = () =>
    this.setState({
      showModal: false
    });

  addUser = (user) => {
    const lastUser = this.state.users[this.state.users.length - 1];
  
    let newId;
    if (lastUser === undefined) {
      newId = 1;
    } else {
      newId = lastUser.id + 1
    }

    const newUser = {
      id: newId,
      name: user.name,
      username: user.username,
      email: user.email,
      address: { city: user.city }
    };

    this.setState(state => ({
      users: [...state.users, newUser],
      showListUsers: !this.state.showListUsers
    }));

  };

  editUser = (dateToEdit) => {
    const users = this.state.users.filter(user => (user.id === this.state.idToEdit ? (
      user.name = dateToEdit.name,
      user.username = dateToEdit.username,
      user.address.city = dateToEdit.city,
      user.email = dateToEdit.email) : true))

    this.setState({
      users: users,
      showListUsers: !this.state.showListUsers
    });

  };

  render() {

    const users = this.state.users.map(user => (
      <User
        openDeleteModal={this.openDeleteModal}
        openEditForm={this.openEditForm}
        user={user}
        key={user.id} />
    ));

    return (
      <>
        <div className="title">Dashboard</div>
        <ModalDelete
          visible={this.state.showModal}
          close={this.closeDeleteModal}
          deleteUser={this.deleteUser}
          nameUserToDelete={this.state.nameUserToDelete}
        />
        {this.state.showListUsers ? (
          <UserList
            users={users}
            handleClick={this.handleRemoveClick}
            handleAddClick={this.handleAddClick}
            openEditForm={this.openEditForm}
          />
        ) : (
            <Form
              userToEdit={this.state.userToEdit}
              type={this.state.typeForm}
              handleCancelClick={this.handleCancelClick.bind(this)}
              onSubmitAdd={this.addUser}
              onSubmitEdit={this.editUser}
            />
          )}
        {this.state.users.length === 0 && this.state.showListUsers && this.state.isLoadingData ?
          <Alert
            className="info-empty-list"
            variant="info">
            Your list of users is empty. Add someone!
          </Alert> : ""
        }
      </>
    );
  }
}

const ModalDelete = ({ visible, close, deleteUser, nameUserToDelete }) => {
  return (
    <>
      <Modal show={visible} onHide={close}>
        <Modal.Header >
          <Modal.Title>Warning!</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you want to remove user <strong>{nameUserToDelete}</strong>?</Modal.Body>
        <Modal.Footer>
          <Button className="cancel-button" variant="secondary" onClick={close}>
            cancel
          </Button>
          <Button className="delete-button" variant="primary" onClick={deleteUser}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default App;
