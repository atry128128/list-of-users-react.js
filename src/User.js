import React from "react";
import "./styles.css";
import { Button } from "react-bootstrap";

const User = props => {
    const user = props.user;
    return (
        <tr>
            <td>{user.id}</td>
            <td>{user.name}</td>
            <td>{user.username}</td>
            <td>{user.email}</td>
            <td>{user.address.city}</td>
            <td><Button onClick={() => props.openEditForm(user)}>Edit</Button></td>
            <td><Button className="delete-button" onClick={() => props.openDeleteModal(user.id)}>Delete</Button></td>
        </tr>
    );
};

export default User;
