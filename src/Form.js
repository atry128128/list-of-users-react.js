import React, { Component, useState } from "react";
import "./styles.css";
import { Form, Button, Col, InputGroup } from "react-bootstrap";

class FormAdd extends Component {

  constructor(props) {
    super(props);
    this.handleCancelClick = props.handleCancelClick;
    this.onSubmitAdd = props.onSubmitAdd;
    this.onSubmitEdit = props.onSubmitEdit;
    this.state = {
      name: "",
      username: "",
      email: "",
      city: ""
    };
  }

  handleChange(name, event) {
    this.setState({
      [name]: event.target.value
    });
  }

  addUser = () => {
    this.props.onSubmitAdd(this.state);
  };

  editUser = () => {
    this.props.onSubmitEdit(this.state);
  }

  render() {
    return (
      <>
        <div className="name-of-page">form to {this.props.type} user</div>
        <FormExample
          valueName={this.state.name}
          valueUserName={this.state.username}
          valueCity={this.state.city}
          valueEmail={this.state.email}
          onChangeName={this.handleChange.bind(this, "name")}
          onChangeUsername={this.handleChange.bind(this, "username")}
          onChangeCity={this.handleChange.bind(this, "city")}
          onChangeEmail={this.handleChange.bind(this, "email")}
          addUser={this.addUser}
          editUser={this.editUser}
          userToEdit={this.props.userToEdit}
          type={this.props.type}
        />
        <Button className="cancel-button cancel-button-form" onClick={this.handleCancelClick}>Cancel</Button>
      </>
    );
  }
}

function FormExample(props) {

  const [validated, setValidated] = useState(false);

  const handleSubmit = event => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      if (props.type === "edit") {
        props.editUser();
      } else {
        props.addUser();
      }
    }
    setValidated(true);
  };

  return (
    <Form className="forms" noValidate validated={validated} onSubmit={handleSubmit}>
      <Form.Row className="justify-content-md-center">
        <Form.Group as={Col} md="4" controlId="validationCustom01">
          <Form.Label><strong>Name:</strong></Form.Label>
          <Form.Control
            required
            type="text"
            value={props.valueName}
            onChange={props.onChangeName}
            placeholder={props.type === "edit" ? props.userToEdit.name : ""}
          />
          <Form.Control.Feedback type="invalid">
            <strong className="info">Name is required! </strong>
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="4" controlId="validationCustom02">
          <Form.Label><strong>Username:</strong></Form.Label>
          <Form.Control
            type="text"
            value={props.valueUserName}
            onChange={props.onChangeUsername}
            placeholder={props.type === "edit" ? props.userToEdit.username : ""}
          />
        </Form.Group>
      </Form.Row>
      <Form.Row className="justify-content-md-center">
        <Form.Group as={Col} md="4" controlId="validationCustom03">
          <Form.Label><strong>City:</strong></Form.Label>
          <Form.Control
            type="text"
            value={props.valueCity}
            onChange={props.onChangeCity}
            placeholder={props.type === "edit" ? props.userToEdit.address.city : ""}
          />
        </Form.Group>
        <Form.Group as={Col} md="4" controlId="validationCustomUsername">
          <Form.Label><strong>E-mail:</strong></Form.Label>
          <InputGroup>
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control
              type="email"
              required
              value={props.valueEmail}
              onChange={props.onChangeEmail}
              placeholder={props.type === "edit" ? props.userToEdit.email : ""}
            />
            <Form.Control.Feedback type="invalid"><strong className="info">E-mail is required!<br />Please use valid e-mail addres e.g. your@mail.com</strong></Form.Control.Feedback>
          </InputGroup>
        </Form.Group>
      </Form.Row>
      <div className="button-cointainer"><Button className="accept-button" type="submit">{props.type} user</Button></div>
    </Form>
  );
}

export default FormAdd;
