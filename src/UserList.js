import React from "react";
import { Table, Button } from "react-bootstrap";
import "./styles.css";

const UserList = props => {
  return (
    <>
      <div className="name-of-page">users list</div>
      <Button
        className="button-add border-button"
        onClick={() => props.handleAddClick()}
      >
        Add new user
      </Button>
      <div className="table-container">
        <Table striped bordered hover variant="dark">
          <thead>
            <tr className="first-row">
              <th>ID</th>
              <th>Name</th>
              <th>Username</th>
              <th>E-mail</th>
              <th>City</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>{props.users}</tbody>
        </Table>
      </div>
    </>
  );
};
export default UserList;
